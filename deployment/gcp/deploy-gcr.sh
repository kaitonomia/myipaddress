#!/bin/bash

echo Starting $0

source $(dirname $0)/settings.env.sh

gcloud config set run/platform managed

# Create a service account for the service
gcloud iam service-accounts create $SERVICE_ACCOUNT_ID \
    --description="MyIPAddress Service account" \
    --display-name="sa my ip address"

# Deploy the cloud run service from the docker image.
echo gcloud run deploy ${SERVICE_NAME} \
    --project  ${PROJECT} \
    --image "${IMAGE}" \
    --platform managed \
    --region ${REGION} \
    --memory=${MEMORY} \
    --timeout=${TIMEOUT} \
    --allow-unauthenticated 

gcloud run deploy ${SERVICE_NAME} \
    --project  ${PROJECT} \
    --image "${IMAGE}" \
    --platform managed \
    --region ${REGION} \
    --memory=${MEMORY} \
    --timeout=${TIMEOUT} \
    --allow-unauthenticated 
    # --service-account=$SERVICE_ACCOUNT 
    # --set-env-vars=AUTH_TOKEN=$APIKEY \
    # --no-allow-unauthenticated \

