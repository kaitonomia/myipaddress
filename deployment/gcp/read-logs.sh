#!/bin/bash

echo Starting $0

source $(dirname $0)/settings.env.sh

gcloud config set run/platform managed

# gcloud logging read "resource.type=cloud_run_revision AND resource.labels.service_name=SERVICE" --project $PROJECT_ID --limit 10
gcloud logging read "resource.type=cloud_run_revision" --project $PROJECT_ID --limit 10
