package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"net"
	"net/http"
	"os"
	"os/signal"
	"sort"
	"strings"
	"sync"
	"sync/atomic"
	"text/template"
	"time"

	"github.com/docker/docker/pkg/namesgenerator"
)

type ConnectionWatcher struct {
	n int64
}

// OnStateChange records open connections in response to connection
// state changes. Set net/http Server.ConnState to this method
// as value.
func (cw *ConnectionWatcher) OnStateChange(conn net.Conn, state http.ConnState) {
	switch state {
	case http.StateNew:
		atomic.AddInt64(&cw.n, 1)
	case http.StateHijacked, http.StateClosed:
		atomic.AddInt64(&cw.n, -1)
	}
}

// Count returns the number of connections at the time
// the call.
func (cw *ConnectionWatcher) Count() int {
	return int(atomic.LoadInt64(&cw.n))
}

type clientInfo struct {
	Port              string              `json:"port,omitempty"`
	RequestURI        string              `json:"requestURI,omitempty"`
	RemoteAddr        string              `json:"remoteAddr,omitempty"`
	UserAgent         string              `json:"userAgent,omitempty"`
	Header            map[string][]string `json:"header,omitempty"`
	Host              string              `json:"host,omitempty"`
	Method            string              `json:"method,omitempty"`
	Body              []byte              `json:"body,omitempty"`
	ClientCertSubject string              `json:"clientCertSubject,omitempty"`
	OpenConnections   int                 `json:"openConnections,omitempty"`
}

func writeClientTemplate(w io.Writer, ci *clientInfo, tpl *template.Template) {
	tpl.Execute(w, ci)
}

func sortHeader(header map[string][]string) []string {
	var res = make([]string, 0, len(header))
	for k := range header {
		res = append(res, k)
	}
	sort.Strings(res)
	return res
}

func writeClientJSON(w io.Writer, ci *clientInfo) {
	resp := struct {
		ClientInfo *clientInfo `json:"clientInfo,omitempty"`
	}{ci}
	enc := json.NewEncoder(w)
	enc.Encode(resp)
}

func writeClientText(w io.Writer, ci *clientInfo) {
	fmt.Fprintln(w, "Instance info:")
	fmt.Fprintf(w, "    Instance:         %s\n", instanceName)
	fmt.Fprintf(w, "    Server hostname:  %s\n", hostname)
	fmt.Fprintf(w, "    Open connections: %d\n", ci.OpenConnections)
	fmt.Fprintln(w, "Request info:")
	if ci.ClientCertSubject != "" {
		fmt.Fprintf(w, "    Client Cert Subject: %s\n", ci.ClientCertSubject)
	}
	fmt.Fprintf(w, "    Host:                %s\n", ci.Host)
	fmt.Fprintf(w, "    Method:              %s\n", ci.Method)
	fmt.Fprintf(w, "    Request uri:         %s\n", ci.RequestURI)
	fmt.Fprintf(w, "    Listen port:         %s\n", ci.Port)
	fmt.Fprintf(w, "    Remote address:      %s\n", ci.RemoteAddr)
	fmt.Fprintf(w, "    UserAgent:           %s\n", ci.UserAgent)

	fmt.Fprint(w, "Headers:\n")
	keys := sortHeader(ci.Header)
	for _, k := range keys {
		fmt.Fprintf(w, "    %s: ", k)
		var first = true
		for _, vv := range ci.Header[k] {
			fmt.Fprintf(w, "%s", vv)
			if !first {
				fmt.Fprint(w, ", ")
			}
			first = false
		}
		fmt.Fprint(w, "\n")
	}
	fmt.Fprint(w, "Body: ")
	if ci.Body == nil {
		fmt.Fprintln(w, "nil")
	} else {
		fmt.Fprintf(w, "size=%d\n%s\n", len(ci.Body), string(ci.Body))
	}

}

func getClientCertSubjectString(r *http.Request) string {
	if r.TLS == nil {
		return ""
	}
	if len(r.TLS.PeerCertificates) == 0 {
		return ""
	}
	return r.TLS.PeerCertificates[0].Subject.ToRDNSequence().String()
}

func (s *server) getClientInfo(r *http.Request) *clientInfo {
	var body []byte
	if r.Body != nil {
		b, _ := ioutil.ReadAll(r.Body)
		body = b
	}
	return &clientInfo{
		RequestURI:        r.RequestURI,
		RemoteAddr:        r.RemoteAddr,
		UserAgent:         r.UserAgent(),
		Header:            r.Header,
		Port:              port,
		Host:              r.Host,
		Method:            r.Method,
		Body:              body,
		ClientCertSubject: getClientCertSubjectString(r),
		OpenConnections:   s.connectionWatcher.Count(),
	}
}

var loadMe float64

func (s *server) handleRequestInfoWithLoad(w http.ResponseWriter, r *http.Request) {

	// busy wait for 500m
	after := time.After(500 * time.Millisecond)
Continue:
	for {
		select {
		case <-after:
			break Continue
		default:
			r := rand.ExpFloat64()
			loadMe = r
		}
	}

	ci := s.getClientInfo(r)
	switch strings.ToLower(r.Header.Get("Accept")) {
	case "application/json":
		w.Header().Add("Content-Type", "application/json")
		writeClientJSON(w, ci)
	default:
		w.Header().Add("Content-Type", "text/plain")
		writeClientText(w, ci)
	}
}

func (s *server) handleRequestInfo(w http.ResponseWriter, r *http.Request) {
	ci := s.getClientInfo(r)
	switch strings.ToLower(r.Header.Get("Accept")) {
	case "application/json":
		w.Header().Add("Content-Type", "application/json")
		writeClientJSON(w, ci)
	default:
		w.Header().Add("Content-Type", "text/plain")
		writeClientText(w, ci)
	}
}

func (s *server) logRequestInfo(w http.ResponseWriter, r *http.Request) {
	ci := s.getClientInfo(r)
	s.logMutex.Lock()
	defer s.logMutex.Unlock()
	writeClientText(os.Stdout, ci)
	os.Stdout.Write([]byte("\n============================\n"))
}

func logInterfaces() {
	ifaces, err := net.Interfaces()
	if err != nil {
		log.Printf("error getting the interfaces: %s", err)
	}
	var (
		netType    = "network"
		ipAddrType = "ip address"
	)
	for n, i := range ifaces {
		addrs, err := i.Addrs()
		if err != nil {
			log.Printf("error getting interface %d: %s", n, err)
		}
		for _, addr := range addrs {
			var addrType *string
			var ip net.IP
			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
				addrType = &ipAddrType
			case *net.IPAddr:
				ip = v.IP
				addrType = &netType
			}
			log.Printf("found interface with %s: %s", *addrType, ip.String())
		}
	}
}

func (s *server) healthHandler(startDelay, healthSeconds int) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		running := time.Now().Sub(startedAt)
		if startDelay > 0 && running.Seconds() < float64(startDelay) {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("starting up..."))
			return
		}
		if healthSeconds > 0 && running.Seconds() > float64(healthSeconds) {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("system error..."))
			return
		}
		w.WriteHeader(http.StatusOK)
		w.Write([]byte("ok"))
	}
}

func (s *server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.handler.ServeHTTP(w, r)
}

func getInstanceName(arg string) string {
	if len(arg) > 0 && strings.ToUpper(arg) != "RANDOM" {
		return arg
	}
	return namesgenerator.GetRandomName(0)
}

const (
	serverShutdownTimeout = 30
)

type server struct {
	logMutex          sync.Mutex
	handler           http.Handler
	connectionWatcher *ConnectionWatcher
}

var instanceName string = "instance"
var port string = "8080"
var hostname string
var startedAt time.Time
var startUpSeconds = 5
var stopAfterSeconds = 0

func main() {
	rand.Seed(time.Now().UnixNano())
	startedAt = time.Now()
	logInterfaces()
	// Determine the port.
	var err error
	if hostname, err = os.Hostname(); err != nil {
		os.Exit(1)
	}
	if p := os.Getenv("PORT"); len(p) > 0 {
		port = p
	}
	la := "0.0.0.0:" + port
	instanceName = getInstanceName(os.Getenv("NAME"))

	mux := http.NewServeMux()
	server := &server{
		handler:           mux,
		connectionWatcher: &ConnectionWatcher{},
	}

	mux.HandleFunc("/", server.handleRequestInfo)
	mux.HandleFunc("/nifi/", server.logRequestInfo)
	mux.HandleFunc("/health", server.healthHandler(startUpSeconds, stopAfterSeconds))
	mux.HandleFunc("/load", server.handleRequestInfoWithLoad)

	// Setup server and done channel
	httpServer := &http.Server{
		Addr:      la,
		Handler:   server,
		ConnState: server.connectionWatcher.OnStateChange,
	}
	httpServer.SetKeepAlivesEnabled(true)
	done := make(chan error, 1)

	// Setup signal catch.
	signals := make(chan os.Signal, 1)
	signal.Notify(signals, os.Interrupt, os.Kill)

	// Start server.
	go func() {
		log.Printf("go: starting server on http://localhost:%s", port)
		done <- httpServer.ListenAndServe()
		log.Print("go: server stopped")
	}()

	// Monitor the signals.
	select {
	case err := <-done:
		log.Printf("server stopped with error: %s", err)
	case s := <-signals:
		log.Printf("signal caught: %s", s.String())
		log.Print("shutting down")
		ctx, cancel := context.WithTimeout(context.Background(), serverShutdownTimeout*time.Second)
		defer cancel()
		err := httpServer.Shutdown(ctx)
		if err != nil {
			log.Printf("server shutdown with error: %s", err)
			break
		}
		log.Print("server shutdown normally")
	}
	os.Exit(0)
}
